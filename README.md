# Pim 2020

### Requisitos

Instalar Java 11 e o gerenciador de pacotes Maven


É necessário ter um banco de dados sql server rodando. As configurações do seu banco local vc pode alterar no arquivo
application.properties, localizado na pasta src > main > resources

Para rodar por linha de comando, compile o código com

``` mvn clean install ```

E para rodar

``` mvn spring-boot:run ```


### Arquitetura

O pacote controller é responsável pelas requisições e roteamentos de páginas (as páginas estáticas se encontram em
resources > templates

O pacote model é responsável pelas classes de banco de dados e dtos

O pacote service é responsável pelas regras de negócio

O pacote repository pelo acesso ao banco de dados, note que ainda não precisamos escrever sql na mão, pois o Hibernate
(uma dependencia que será instalada junto com o projeto pelo maven) cria essa abstração pra gnt