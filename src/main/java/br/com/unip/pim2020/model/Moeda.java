package br.com.unip.pim2020.model;

import java.math.BigInteger;
import java.time.LocalDateTime;


//@Entity
public class Moeda {

//    @Id
    private Integer id;
    private String nome;
    private BigInteger cotacaoEmReais; // arrumar data type
    private LocalDateTime dataCriacao;

    public Moeda() {
    }

    public Moeda(Integer id, String nome, BigInteger cotacaoEmReais, LocalDateTime dataCriacao) {
        this.id = id;
        this.nome = nome;
        this.cotacaoEmReais = cotacaoEmReais;
        this.dataCriacao = dataCriacao;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public BigInteger getCotacaoEmReais() {
        return cotacaoEmReais;
    }

    public void setCotacaoEmReais(BigInteger cotacaoEmReais) {
        this.cotacaoEmReais = cotacaoEmReais;
    }

    public LocalDateTime getDataCriacao() {
        return dataCriacao;
    }

    public void setDataCriacao(LocalDateTime dataCriacao) {
        this.dataCriacao = dataCriacao;
    }
}
