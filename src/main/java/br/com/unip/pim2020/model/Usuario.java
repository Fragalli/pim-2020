package br.com.unip.pim2020.model;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;

@Entity
public class Usuario {

    @Id
    private Integer id;
    private String nome;
    @Enumerated(EnumType.STRING)
    private Perfil perfil;
    private String login;
    private String senha;

    public Usuario() {
    }

    public Usuario(Integer id, String nome, Perfil perfil, String login, String senha) {
        this.id = id;
        this.nome = nome;
        this.perfil = perfil;
        this.login = login;
        this.senha = senha;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Perfil getPerfil() {
        return perfil;
    }

    public void setPerfil(Perfil perfil) {
        this.perfil = perfil;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }
}
