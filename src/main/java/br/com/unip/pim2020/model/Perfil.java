package br.com.unip.pim2020.model;

public enum Perfil {
    ADMINISTRADOR,
    PARCEIRO,
    CLIENTE
}
