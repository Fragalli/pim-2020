package br.com.unip.pim2020;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Pim2020Application {

	public static void main(String[] args) {
		SpringApplication.run(Pim2020Application.class, args);
	}

}
