package br.com.unip.pim2020.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.unip.pim2020.model.Usuario;
import br.com.unip.pim2020.repository.UsuarioRepository;

@Service
public class UsuarioService {

    @Autowired
    private UsuarioRepository repository;

    public List<Usuario> listaTodos() {
        return repository.findAll();
    }
}
