package br.com.unip.pim2020.controller;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import br.com.unip.pim2020.model.Produto;

@Controller
@RequestMapping("/produtos")
public class ProdutosController {

    private static final List<Produto> DADOS = Arrays.asList(
            new Produto(1L, "iPhone 11", BigDecimal.valueOf(4500)),
            new Produto(2L, "Notebook Dell Latitude", BigDecimal.valueOf(2900)),
            new Produto(3L, "TV LG 55\"", BigDecimal.valueOf(3000)),
            new Produto(4L, "Criptomoeda XYZ", BigDecimal.valueOf(15))
    );


    @GetMapping
    public ModelAndView lista() {
        ModelAndView model = new ModelAndView("/produtos");

        model.addObject("produtos", DADOS);

        return model;
    }
}
