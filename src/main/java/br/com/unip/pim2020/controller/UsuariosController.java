package br.com.unip.pim2020.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import br.com.unip.pim2020.service.UsuarioService;

@Controller
@RequestMapping("/usuarios")
public class UsuariosController {

    @Autowired
    private UsuarioService usuarioService;

    @GetMapping
    public ModelAndView lista(){
        ModelAndView model = new ModelAndView("/usuarios");

        model.addObject("usuarios", usuarioService.listaTodos());

        return model;
    }
}
